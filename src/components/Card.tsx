import React, { FC, useState } from 'react';

export enum CardVariant {
  outlined = 'outlined',
  primary = 'primary',
}
interface CardProps {
  width: string;
  height?: string;
  variant: CardVariant;
  onClick: (num: number) => void; // void значит функция ничего не должна возвращать, string значит функция должна вернуть строку и т.д.
}

const Card: FC<CardProps> = ({ width, height, variant, children, onClick }) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [state, setState] = useState(0);
  return (
    <div
      style={{
        width,
        height,
        border: variant === CardVariant.outlined ? '1px solid grey' : 'none',
        background: variant === CardVariant.primary ? 'lightgray' : '',
      }}
      onClick={() => onClick(state)}
    >
      {children}
    </div>
  );
};

export default Card;
