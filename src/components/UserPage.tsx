import axios from 'axios';
import React, { FC, useEffect, useState } from 'react';
import { IUser } from '../types/types';
import List from './List';
import UserItem from './UserItem';
import { useNavigate } from 'react-router-dom';

const UserPage: FC = () => {
  const [users, setUsers] = useState<IUser[]>([]);

  const navigate = useNavigate();

  useEffect(() => {
    fetchUsers();
  }, []);

  async function fetchUsers() {
    try {
      const response = await axios.get<IUser[]>(
        'https://jsonplaceholder.typicode.com/users'
      );
      setUsers(response.data);
    } catch (error) {
      alert(error);
    }
  }

  return (
    <>
      <h2>Список пользователей на компоненте List</h2>
      <List
        items={users}
        renderItem={(user: IUser) => (
          <UserItem
            key={user.id}
            user={user}
            onClick={(user) => {
              navigate('/users/' + user.id);
            }}
          />
        )}
      />
    </>
  );
};

export default UserPage;
